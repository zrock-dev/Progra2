package circular_linked_list;

public class CircularLinkedList<T extends Comparable<T>> implements List<T>{
    private int size;
    private Node<T> head;
    private Node<T> root;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null && root == null;
    }

    @Override
    public boolean add(T data) {
        if (root == null){
            root = new Node<>(data, null, null);
            head = root;
            head.setAfter(root.getAfter());
        }else {
            Node<T> temp = head;
            head = new Node<>(data, temp, root);
            temp.setNext(head);
            root.setAfter(head);
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(T data) {
        if (root.getData().equals(data)){
            root = root.getNext();
            head.setNext(root);
        }else {
            Node<T> nodeToRemove = getNode(data);
            Node<T> after = nodeToRemove.getAfter();
            after.setNext(nodeToRemove.getNext());
        }

        size--;
        return true;
    }

    @Override
    public T get(int index) {
        if (index < 0) return null;
        Node<T> node = root;
        for (int i = 0; i < index; i++, node = node.getNext());

        return node.getData();
    }

    @Override
    public void selectionSort() {
        for (int i = 0; i < size; i++) {
            Node<T> base = node(i);
            Node<T> min = base;

            int minIndex = i;
            for (int j = i; j < size; j++){
                if (min.getData().compareTo(base.getData()) > 0) {
                    min = base;
                    minIndex = j;
                }
                base = base.getNext();
            }
            swapNodes(i, minIndex);
        }
        updatePointers();
    }

    @Override
    public void bubbleSort() {
        for (int i = 0; i < size() - 1; i++) {
            Node<T> heavyNode = node(0);
            // -1 less than
            // 0 equal to
            // 1 greater than
            for (int j = 0; j < size() - 1; j++) {
                if (heavyNode.getData().compareTo(heavyNode.getNext().getData()) < 0){
                    heavyNode = heavyNode.getNext();
                }else {
                    swapNodes(j, j + 1);
                }
            }
        }
        updatePointers();
    }

    @Override
    public void reverse() {
        for (int i = 0; i < size/2; i++) {
            swapNodes(i, (size-1) - i);
        }
    }

    @Override
    public String toString() {
        String list = "";
        Node<T> node = root;
        for (int index = 0; index < size; index++) {
            list += "-> " + node.getData();
            node = node.getNext();
        }
        return list;
    }

    /**
     * Updates the root and head pointers to the correct nodes.
     */
    private void updatePointers(){
        root = node(0);
        head = node(size() - 1);
    }

    /**
     * Searches in the list for a node which contains the given data.
     * @param data The value of the node required.
     * @return The node with the value required-
     */
    private Node<T> getNode(T data){
        Node<T> node = root;
        for (int i = 0; i < size && !node.getData().equals(data); i++, node = node.getNext());
        return node;
    }

    /**
     * Swaps two nodes, making sure to keep the list integrity
     * by updating the references to such nodes.
     * @param x leftmost element of the list.
     * @param y rightmost element of the list.
     * @throws IllegalArgumentException in the case the y is less than or equal to x,
     * because otherwise the index orientation will be lost.
     */
    private void swapNodes(int x, int y) throws IllegalArgumentException{
        if (y < x) throw new IllegalArgumentException("y: "+ y+ " cant be less than or equal to x:"+ x);

        Node<T> xNode = node(x);
        Node<T> yNode = node(y);
        if (xNode == root && yNode == head){
            yNode.getAfter().setNext(yNode.getNext());
            xNode.getNext().setAfter(yNode);

            Node<T> temp = xNode.getNext();
            xNode.setNext(yNode);
            xNode.setAfter(yNode.getAfter());

            yNode.setNext(temp);
            yNode.setAfter(xNode);
        }
        else if (y - x == 1){
            xNode.setNext(yNode.getNext());
            yNode.getNext().setAfter(xNode);

            yNode.setNext(xNode);
            yNode.setAfter(xNode.getAfter());
            yNode.getAfter().setNext(yNode);
            xNode.setAfter(yNode);
        }
        else {
            Node<T> xNextTemp = xNode.getNext();
            xNode.setNext(yNode.getNext());
            yNode.setNext(xNextTemp);

            Node<T> xAfterTemp = xNode.getAfter();
            xNode.setAfter(yNode.getAfter());
            yNode.setAfter(xAfterTemp);

            xNode.getAfter().setNext(xNode);
            yNode.getAfter().setNext(yNode);

            xNode.getNext().setAfter(xNode);
            yNode.getNext().setAfter(yNode);
        }

        if (xNode == root){
            root = yNode;
        } else if (yNode == root) {
            root = xNode;
        }

        head = node(size - 1);
    }

    /**
     * Loops in the list from the root to the head, looking for the node in
     * the indicated position.
     * @param index position of the node.
     * @return The node located in the given location.
     */
    private Node<T> node(int index){
        Node<T> node = root;
        for (int i = 0; i < index; i++) {
            node = node.getNext();
        }
        return node;
    }
}
