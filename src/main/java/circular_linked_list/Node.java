package circular_linked_list;

public class Node<T> {
    private final T data;
    private Node<T> after;
    private Node<T> next;

    public Node(final T data, Node<T> after, Node<T> next) {
        this.data = data;
        this.after = after;
        this.next = next;
    }

    public T getData() {
        return data;
    }

    public Node<T> getAfter() {
        return after;
    }

    public Node<T> getNext() {
        return next;
    }


    public void setAfter(Node<T> after) {
        this.after = after;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public String toString(){
        return "|"+ after.data+ " <- " + data+ " -> "+ next.data + "|";
    }
}
