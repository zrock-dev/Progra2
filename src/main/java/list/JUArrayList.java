package list;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * The JUArrayList is a class to manage data structures.
 */

public class JUArrayList implements List<Integer> {
    private int[] array;
    private int countOfAddedItems;
    private final int DEFAULT_SIZE;

    public JUArrayList() {
        DEFAULT_SIZE = 10;
        countOfAddedItems = 0;
        array = new int[DEFAULT_SIZE];
    }

    @Override
    public boolean add(Integer item){
        fixArraySize();
        array[countOfAddedItems] = item;
        countOfAddedItems++;
        return true;
    }

    /**
     * Creates a new array with an expanded size from the original array field in the class.
     */
    private void fixArraySize(){
        if (countOfAddedItems == array.length){
            int[] transitiveArray = new int[array.length + DEFAULT_SIZE];

            for (int index = 0; index < array.length; index++) {
                transitiveArray[index] = array[index];
            }

            array = transitiveArray;
        }
    }

    @Override
    public Integer remove(int index){
        int removedNumber = -1;
        if (index <= countOfAddedItems){
            removedNumber = array[index];
            for (int i = index; i < countOfAddedItems; i++) {
                array[i] = array[i + 1];
            }
            countOfAddedItems--;
        }
        return removedNumber;
    }

    @Override
    public int size() {
        return countOfAddedItems;
    }

    @Override
    public boolean isEmpty() {
        return countOfAddedItems == 0;
    }

    @Override
    public boolean contains(Object number) {
        boolean evaluation = false;
        for (int index = 0; index < countOfAddedItems; index++) {
            if (number.equals(array[index])){
                evaluation = true;
            }
        }
        return evaluation;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<>() {
            private int count;

            @Override
            public boolean hasNext() {
                return count <= countOfAddedItems && countOfAddedItems > 0;
            }

            @Override
            public Integer next() {
                int number = array[count];
                count++;
                return number;
            }
        };
    }

    @Override
    public Object[] toArray() {
        Object[] sortedArray = new Object[this.size()];
        for (int index = 0; index < this.size(); index++) {
            sortedArray[index] = array[index];
        }
        return sortedArray;
    }

    @Override
    public String toString(){
        String arrayOfStrings = "";
        for (int number:
                array) {
            arrayOfStrings += number + " ";
        }
        return arrayOfStrings;
    }

    @Override
    public boolean remove(Object o) {
        boolean evaluation = false;
        int indexToRemove = indexOf(o);
        if (indexToRemove != -1){
            remove(indexToRemove);
            evaluation = true;
        }
        return evaluation;
    }

    @Override
    public int indexOf(Object o) {
        int numberIndex = -1;
        for (int index = 0; index < countOfAddedItems; index++) {
            if (o.equals(array[index])){
                numberIndex = index;
                break;
            }
        }
        return numberIndex;
    }

    @Override
    public void clear() {
        array = new int[DEFAULT_SIZE];
        countOfAddedItems = 0;
    }

    @Override
    public Integer get(int index){
        int number = -1;
        if (index <= countOfAddedItems){
            number = array[index];
        }
        return number;
    }

    @Override
    public Integer set(int index, Integer element) {
        int previousNumber = array[index];
        array[index] = element;
        return previousNumber;
    }

    @Override
    public void add(int index, Integer element) {
        if (index <= countOfAddedItems){
            int lastCountIndex = countOfAddedItems -1;
            countOfAddedItems++;
            fixArraySize();

            for (int i = lastCountIndex ; i >= index ; i--) {
                array[i + 1] = array[i];
            }
            array[index] = element;
        }
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }
}
