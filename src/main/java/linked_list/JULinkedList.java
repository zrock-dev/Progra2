package linked_list;
import list.JUArrayList;

import java.util.*;

public class JULinkedList<T> implements List<T> {
    private Node<T> head;
    private Node<T> root;
    private int size;

    /**
     * The Node class saves data and a reference to other node.
     * @param <T> Type of data to save.
     */
    private static class Node<T>{
        T data;
        Node<T> next;

        Node(Node<T> next, T data){
            this.data = data;
            this.next = next;
        }

        @Override
        public String toString() {
            return String.valueOf(data);
        }
    }

    @Override
    public int size(){
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size <= 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            private Node<T> baseNode = root;

            @Override
            public boolean hasNext() {
                return baseNode.next != null;
            }

            @Override
            public T next() {
                Node<T> lastNode = baseNode;
                baseNode = baseNode.next;
                return lastNode.data;
            }
        };
    }

    @Override
    public boolean add(T t) {
        Node<T> lastNode = head;
        Node<T> newNode = new Node<>(null, t);
        head = newNode;
        if (root == null)
            root = newNode;
        else
            lastNode.next = newNode;

        size++;
        return true;
    }

    @Override
    public boolean remove(Object data) {

        if(isEmpty()) return false;

        if (data.equals(root.data)){
            root = root.next;
            size--;
            return true;
        }


        for (Node<T> currentNode = root;
             currentNode.next != null;
             currentNode = currentNode.next){

            if (data.equals(currentNode.next.data)){
                currentNode.next = currentNode.next.next;
                size--;
                return true;
            }
        }

        return false;
    }

    @Override
    public void clear() {
        size = 0;
        head = null;
        root = null;
    }

    @Override
    public T get(int index) {
        Node<T> node = getNode(index);
        if (node != null){
            return node.data;
        }
        return null;
    }

    @Override
    public T remove(int index){
        if (index > size) return null;

        if (index == 0){
            T data = root.data;
            root = root.next;
            return data;
        }

        Node<T> current = root;

        for (int i = 0; i < index; i++) {
            current = current.next;
        }

        if (index == size){
            current.next = null;
        }else {
            current.next = current.next.next;
        }

        size--;
        return current.data;
    }

    @Override
    public boolean contains(Object o) {
        Node<T> node = root;
        for (int i = 0; i < size; i++) {
            if (node.data.equals(o)){
                return true;
            }else node = node.next;
        }
        return false;
    }

    @Override
    public T set(int index, T element) {
        if (index < size) return null;

        Node<T> node = getNode(index);
        assert node != null;
        T previousValue = node.data;
        node.data = element;
        return previousValue;
    }

    @Override
    public int indexOf(Object o) {
        int iterations = 0;
        for (Node<T> node = root; node.next != null; node= node.next){
            if (o.equals(node.data)){
                return iterations;
            }
            iterations++;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<T> current = root;
        int lastIndex = 0;
        for (int i = 0; i < size; i++) {
            if (current.data.equals(0)){
                lastIndex = i;
            }else {
                current = current.next;
            }
        }
        return lastIndex;
    }

    @Override
    public void add(int index, T element) {
        Node<T> current = root;
        for (int i = 0; i < index -1; i++) {
            current = current.next;
        }

        Node<T> temp = current;
        current.next = new Node<>(temp.next, element);
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        Node<T> node = root;
        for (int i = 0; i < size; i++) {
            array[i] = node.data;
            node = node.next;
        }
        return array;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (fromIndex == toIndex) return new JULinkedList<>();

        Node<T> tail = getNode(fromIndex);
        JULinkedList<T> list = new JULinkedList<>();
        int iterationsLimit = toIndex - fromIndex;
        for (int i = 0; i < iterationsLimit; i++) {
            assert tail != null;
            list.add(tail.data);
            tail = tail.next;
        }

        return list;
    }

    /**
     * This method returns a node in the required index position.
     * @param index required position
     * @return Node in the position
     */
    private Node<T> getNode(int index){
        if (index < size){
            Node<T> current = root;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current;
        }else return null;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public String toString() {
        String nodeListInformation = "";
        for (Node<T> node = root; node != null; node = node.next) {
            nodeListInformation += " -> " + node.data;
        }

        return nodeListInformation;
    }
}
