package clase0811.bags;

public interface Bag<T extends Comparable<T> > {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();

    /**
     * changes the x element position to y position and
     *     y element position to x position
     *     <p>
     *     0 1 2 3 4
     *      <p>
     *     1,2,3,4,5
     *     <p>
     *     xchange(1,3) =>
     *     <p>
     *     1,4,3,2,5
     *
     * @param y element position
     * @param x element position
     */
    void xchange(int y, int x);

}
class Node<T> {
    private T data;
    private Node<T> next;

    public Node(T data) { this.data = data; }

    public T getData() { return data;}

    public Node<T> getNext() { return next; }

    public void setNext(Node<T> node) { next = node; }

    public String toString() {
        return data + " -> " + next;
    }
}

class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;

    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }
    public String toString() {
        return root.toString();
    }

    public void selectionSort() {
        Node<T> shiftedList = root;
        int shift = 0;

        while (shiftedList.getNext() != null){
            Node<T> min = shiftedList;

            for (Node<T> head = shiftedList;
                 head.getNext() != null;
                 head = head.getNext()) {

                Node<T> nextNode = head.getNext();
                if (min.getData().compareTo(nextNode.getData()) > 0){
                    min = nextNode;
                }
            }

            try {
                xchange(
                        indexOf(min),
                        indexOf(shiftedList)
                );
            }catch (IllegalArgumentException ignored){};

            shiftedList = root;
            shift++;
            for (int i = 0; i < shift; i++) shiftedList = shiftedList.getNext();
        }

    }

    public void bubbleSort() {

    }

    private Node<T> getReferenceToNode(Node<T> node, Node<T> base){
        Node<T> head;
        for (head = base; head.getNext() != null; head = head.getNext()) {
            if (head.getNext().equals(node)){
                break;
            }
        }
        return head;
    }

    private int indexOf(Node<T> o){
        int index = 0;
        for (Node<T> node = root; node != o; node = node.getNext()){
            index++;
        }
        return index;
    }

    @Override
    public void xchange(int y, int x) throws IllegalArgumentException {

        if (!(x < y)) throw new IllegalArgumentException(y + " cant be less or equal to: " + x);

        Node<T> shiftedArray = root;
        Node<T> anchorNode = null;
        if (y > 1){
            for (int i = 0; i < x; i++ , shiftedArray = shiftedArray.getNext()){
                y--;
            }
            anchorNode = getReferenceToNode(shiftedArray, root);
        }

        Node<T> nodeX  = shiftedArray;
        Node<T> nodeY = shiftedArray;
        for (int i = 0; i < y; i++) {
            nodeY = nodeY.getNext();
        }

        if(y == 1){
            Node<T> ref = nodeY.getNext();
            nodeX.setNext(null);
            nodeY.setNext(nodeX);
            nodeX.setNext(ref);
        }

        if (y > 1){
            Node<T> ref = nodeY.getNext();
            Node<T> tNode;
            for (tNode = nodeX.getNext(); tNode.getNext() != nodeY; tNode = tNode.getNext());
            tNode.setNext(null);

            nodeY.setNext(nodeX.getNext());
            nodeX.setNext(ref);
            tNode.setNext(nodeX);
        }

        if (anchorNode != null) {
            if (x == 0){
                root = anchorNode;
            }else anchorNode.setNext(nodeY);
        }else {
            root = nodeY;
        }
    }
}