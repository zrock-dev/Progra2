package nodes;

public class NodeA {
    public int data;
    public NodeA next = null;


    /**
     * This method jumps to the next node until reach of the index.
     * @param n Tail node.
     * @param index node required.
     * @return data of the node required.
     * @throws Exception
     */
    public static int getNth(NodeA n, int index) throws Exception{
        NodeA lastNode = n;

        for (int i = index; i > 0; i--) {
            lastNode = lastNode.next;
        }

        return lastNode.data;
    }
}
