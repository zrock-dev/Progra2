package nodes;

public class Node {

    int data;
    Node next = null;

    Node(final int data) {
        this.data = data;
    }

    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }

    /**
     * Replaces the last null node of the list A with the first node of the list B
     * @param listA The list to append.
     * @param listB The list to be appended.
     * @return The head of the appended list.
     */
    public static Node append(Node listA, Node listB) {

        if(listA == null)
            return listB;
        else if (listB == null) {
            return listA;
        }
        
        Node current = listA;
        while (current.next != null){
            current = current.next;
        }
        current.next = listB;

        return listA;

    }

    public int getData() {
        return data;
    }
}
