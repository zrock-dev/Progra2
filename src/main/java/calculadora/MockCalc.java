package calculadora;

public class MockCalc extends AbstractCalc {
    @Override
    protected void build() {

    }

    @Override
    public int calc(String str) {
        verify();

        return 1;
    }
}
