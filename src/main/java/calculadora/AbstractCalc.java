package calculadora;

import calculadora.exceptions.InvalidTokenException;

public abstract class AbstractCalc {
    protected abstract void build();
    public abstract int calc(String str);
    public boolean verify() {
        try {
            build();
        } catch (InvalidTokenException ex) {
            throw ex;
        }
        return true;
    }
}
