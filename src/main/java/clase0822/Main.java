package clase0822;

import clase0822.recursividad.Contador;
import clase0822.recursividad.Infinito;
import clase0822.recursividad.LlamadasEncadenadas;
import clase0822.recursividad.RecursividadvsIteracion;

public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        //LlamadasEncadenadas.doOne();

        // Descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Lo que sucede, es que el metodo whileTrue se llama a sí mismo sin ninguna restriccion o control.
        // Lo que desencadena en un recursion infinita.
        // 3.- volver a comentar la llamada a: Infinito.whileTrue()
        //Infinito.whileTrue();

        // Des-comentar la llamada a: Contador.contarHasta(), pasando un numero entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // El metodo contarHasta, verifica que el parametro contador sea mayor a cero, si es asi: llama de forma
        // recursiva a sí mismo pasando como parametro un contador-1. Este es un ejemplo de recursion contralada,
        // dado que existe un limite o una condicion para poder entrar o continuar con la recursion.
        //Contador.contarHasta(1);  

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));


    }
}
