package game;

import game.player.PlayerManager;

/**
 * Game class is where the game is controlled.
 */
public class Game {
    private ObstacleGenerator obstacleGenerator;
    private PlayerManager playerManager;
    private ConsoleMessages consoleMessages;
    private final int NUM_OBSTACLES;
    private GameMap gameMap;

    /**
     * This constructor initializes the class fields.
     *
     * @param gameMap map of the game.
     * @param obstacleGenerator the obstacle generator.
     * @param playerManager manager of the player.
     * @param consoleMessages the output in console.
     */
    public Game(GameMap gameMap, ObstacleGenerator obstacleGenerator, PlayerManager playerManager, ConsoleMessages consoleMessages) {
        this.obstacleGenerator = obstacleGenerator;
        this.playerManager = playerManager;
        this.consoleMessages = consoleMessages;
        this.gameMap = gameMap;
        NUM_OBSTACLES = 10;
    }

    /**
     * Fills the map with horizontal or vertical randomly.
     */
    private void populateMap(){
        for (int iteration = 0; iteration < NUM_OBSTACLES; iteration++) {
            if (Utils.flipCoin()){
                obstacleGenerator.horizontalObstacle();
            }else {
                obstacleGenerator.verticalObstacle();
            }
        }
    }

    /**
     * This main method controls the game flow.
     */
    public void run(){
        populateMap();
        consoleMessages.gameTitle();
        do {
            consoleMessages.showPlayerHealthBar(
                    playerManager.getPlayer().getName(),
                    playerManager.getPlayer().getHealthPoints()
            );

            consoleMessages.updateMapView(gameMap.getMap());
            playerManager.movePlayer(consoleMessages.getNextMove());
        }
        while (!playerManager.winStatus() && playerManager.isPlayerAlive());

        consoleMessages.updateMapView(gameMap.getMap());
        if (playerManager.isPlayerAlive()){
            consoleMessages.gameWinMessage(
                    playerManager.getPlayer().getName(),
                    playerManager.getPlayer().getWeapon()
            );
        }else {
            consoleMessages.playerDeadMessage();
        }
    }
}
