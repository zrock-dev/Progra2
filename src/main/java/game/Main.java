package game;

import game.player.Player;
import game.player.PlayerManager;

public class Main {
    /*
     * Implementar un juego que use un mapa (10x10) de coordenadas X Y, donde un jugador será ubicado (en una casilla)
     * en una posición aleatoria dentro el mapa, luego 5 obstáculos dentro el mapa en posiciones aleatorias, similar
     * que el jugador intenta llegar a la esquina inferior derecha del mapa, un paso a la vez, pasando de
     * su posición actual a la derecha, izquierda, arriba, abajo. Siempre y cuando no se encuentre con un obstáculo.
     */

    public static void main(String[] args) {
        GameMap gameMap = new GameMap();
        Player player = new Player("Samurai Champloo", "Katana");
        ConsoleMessages consoleMessages = new ConsoleMessages();
        PlayerManager playerManager = new PlayerManager(player, gameMap, consoleMessages);
        Game game = new Game(
                gameMap,
                new ObstacleGenerator(gameMap),
                playerManager,
                consoleMessages);

        game.run();
    }
}
