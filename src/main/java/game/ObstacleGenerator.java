package game;

import static game.Utils.getRandomCoord;

/**
 * This class ObstacleGenerator creates new obstacles in the map.
 */
public class ObstacleGenerator {
    private int coordA;
    private int coordB;
    private int alignmentAxis;
    private final int MAX_OBSTACLE_LENGTH;
    private GameMap gameMap;

    /**
     * This constructor initializes the class fields.
     *
     * @param gameMap map of the game.
     */
    public ObstacleGenerator(GameMap gameMap) {
        MAX_OBSTACLE_LENGTH = gameMap.size() - 2;
        this.gameMap = gameMap;
    }

    /**
     * generates a vertical obstacle in the map.
     */
    public void verticalObstacle(){
        generateCoords();
        gameMap.drawVLine(coordA, coordB, alignmentAxis);
    }

    /**
     * generates a horizontal obstacle in the map.
     */
    public void horizontalObstacle(){
        generateCoords();
        gameMap.drawHLine(coordA, coordB, alignmentAxis);
    }

    /**
     * Fills three class fields whit random cartesian coordinates, to generate obstacles in the map.
     */
    private void generateCoords(){
        coordA = getRandomCoord(MAX_OBSTACLE_LENGTH);
        coordB = getRandomCoord(MAX_OBSTACLE_LENGTH);
        alignmentAxis = getRandomCoord();
    }
}
