package game;

import java.util.Random;

/**
 * This class contains reusable code.
 */
public class Utils {

    /**
     * By using a random class it generates a random number, which is a coordinate in the map.
     * @return a random number.
     */
    public static int getRandomCoord(){
        return new Random().nextInt(9);
    }

    /**
     * By using a random class it generates a random number, which is a coordinate in the map.
     *
     * @param bound max value.
     * @return a random number.
     */
    protected static int getRandomCoord( int bound){
        return new Random().nextInt(bound);
    }

    /**
     * Generates a random boolean value.
     * @return boolean value.
     */
    protected static boolean flipCoin(){
        return new Random().nextBoolean();
    }
}
