package game.player;
import game.ConsoleMessages;
import game.GameMap;
import game.Utils;

/**
 * Player manager class manages the player, in actions such as move in the map.
 */
public class PlayerManager {
    private ConsoleMessages consoleMessages;
    private GameMap gameMap;
    private Player player;

    private int playerXPos;
    private int playerYPos;

    /**
     * This constructor initializes the class fields.
     *
     * @param player the player of the game.
     * @param gameMap map of the game.
     * @param consoleMessages the output in the screen.
     */
    public PlayerManager(Player player, GameMap gameMap, ConsoleMessages consoleMessages) {
        this.player = player;
        this.gameMap = gameMap;
        this.consoleMessages = consoleMessages;
        initPlayer();
    }

    /**
     * This method checks if the player has reached the win square.
     *
     * @return a logic evaluation of the condition.
     */
    public boolean winStatus(){
        return playerXPos == 9 && playerYPos == 9;
    }

    /**
     * @return the player of the game.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Based on a class field.
     *
     * @return the status of the player.
     */
    public boolean isPlayerAlive(){
        return player.getHealthPoints() > 0;
    }

    /**
     * Here this method changes the coordinates of the player based in the movement.
     *
     * @param movement the keymap is "w" for up, "s" for down, "a" for left and "d" for right.
     */
    public void movePlayer(String movement){
        int xPos = this.playerXPos;
        int yPos = this.playerYPos;
        switch (movement) {
            case "w" -> yPos--;
            case "s" -> yPos++;
            case "a" -> xPos--;
            case "d" -> xPos++;
            default -> consoleMessages.illegalChoice();
        }
        validateStep(xPos, yPos);
    }

    /**
     * This method validates if the player can move to the square.
     * @param xPos y coordinate.
     * @param yPos x coordinate.
     */
    private void validateStep(int xPos, int yPos){
        try {
            if (!gameMap.squareIsObstacle(xPos, yPos)){

                gameMap.clearSquare(this.playerXPos, this.playerYPos);
                gameMap.movePlayer(xPos, yPos);
                saveMovement(xPos, yPos);

            }else {
                playerHitObstacle();
            }
        }catch (IndexOutOfBoundsException ignored){
            consoleMessages.illegalMovement();
        }
    }

    /**
     * Saves the last valid movement the player has made.
     *
     * @param xPos x coordinate.
     * @param yPos y coordinate.
     */
    private void saveMovement(int xPos, int yPos){
        this.playerXPos = xPos;
        this.playerYPos = yPos;
    }

    /**
     * This method manages hit action whenever a player touches a wall.
     */
    private void playerHitObstacle(){
        player.hitPlayer();
        consoleMessages.obstacleHit();
    }

    /**
     * This method initializes the player position in a random location.
     */
    private void initPlayer(){
        playerXPos = Utils.getRandomCoord();
        playerYPos = Utils.getRandomCoord();
        gameMap.movePlayer(playerXPos, playerYPos);
    }
}
