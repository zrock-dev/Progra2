package game.player;

/**
 * PLayer is the representation of the player in the game map.
 * <p>
 * It has a name, a weapon and health points.
 */
public class Player {
    private String name;
    private String weapon;
    private int healthPoints;

    /**
     * This constructor initializes the class fields.
     * @param name name of the player.
     * @param weapon weapon of the player.
     */
    public Player(String name, String weapon) {
        this.name = name;
        this.weapon = weapon;
        healthPoints = 5;
    }

    /**
     * @return the name of the player
     */
    public String getName() {
        return name;
    }

    /**
     * @return the weapon of the player
     */
    public String getWeapon() {
        return weapon;
    }

    /**
     * @return the health points of the player
     */
    public int getHealthPoints(){
        return healthPoints;
    }

    /**
     * Whenever the player touches any obstacle a point of its life is discounted.
     */
    protected void hitPlayer(){
        healthPoints--;
    }
}
