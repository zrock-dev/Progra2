package game;

/**
 * GameMap class manages the game map which are composed by obstacles and player.
 */
public class GameMap {
    private String[][] rows;
    private final String EMPTY_SPACE;
    private final String OBSTACLE;
    private final String PLAYER;

    /**
     * This constructor initializes the class fields.
     */
    public GameMap(){
        rows = new String[10][1];
        EMPTY_SPACE ="·";
        OBSTACLE = "+";
        PLAYER = "O";
        fillMap();
    }

    /**
     * Calculates the height of the map.
     *
     * @return the size of the map.
     */
    public int size(){
        return rows.length;
    }

    /**
     * For any given position to be cleared with the default empty space.
     *
     * @param x x coordinate.
     * @param y y coordinate.
     */
    public void clearSquare(int x, int y){
        rows[y][x] = EMPTY_SPACE;
    }

    /**
     * Moves the player to a specific location.
     *
     * @param x x coordinate.
     * @param y y coordinate.
     */
    public void movePlayer(int x, int y){
        rows[y][x] = PLAYER;
    }

    /**
     * Checks if the square is part of an obstacle.
     *
     * @param x x coordinate.
     * @param y y coordinate.
     * @return true if there is an obstacle and false if is not.
     */
    public boolean squareIsObstacle(int x, int y){
        return rows[y][x].equals(OBSTACLE);
    }

    /**
     * Draws a horizontal obstacle along the map, from one point to another.
     *
     * @param x1 x coordinate of the first point.
     * @param x2 x coordinate of the second point.
     * @param rowNumber y coordinate to draw the obstacle.
     */
    public void drawHLine(int x1, int x2, int rowNumber){
        for (int index = x1; index <= x2; index++) {
            rows[rowNumber][index] = OBSTACLE;
        }
    }

    /**
     * Draws a vertical obstacle along the map, from one point to another.
     *
     * @param y1 y coordinate of the first point.
     * @param y2 y coordinate of the second point.
     * @param columnNumber the column or x position to draw the obstacle.
     */
    public void drawVLine(int y1, int y2, int columnNumber){
        for (int index = y1; index <= y2; index++) {
            rows[index][columnNumber] = OBSTACLE;
        }
    }

    /**
     * This method fills the map with empty spaces in the horizontal lines.
     */
    private void fillMap(){
        String[] columns = new String[10];
        for (int iteration = 0; iteration < 10; iteration++) {
            columns[iteration] = EMPTY_SPACE;
        }

        for (int index = 0; index < rows.length; index++) {
            rows[index] = columns.clone();
        }
    }

    /**
     * To get the map.
     * @return the map nested collections.
     */
    public String[][] getMap(){
        return rows;
    }
}
