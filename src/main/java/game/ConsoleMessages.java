package game;

import java.util.Scanner;

/**
 * ConsoleMessages class prints messages in the console.
 */
public class ConsoleMessages {
    private Scanner sn;

    /**
     * This constructor instantiates the class field.
     */
    public ConsoleMessages() {
        this.sn = new Scanner(System.in);
    }

    /**
     * Prints the header or title of the game.
     */
    public void gameTitle(){
        System.out.println("------- Dungeons & Dragons -------");
    }

    /**
     * Gets the key for the direction to move the player.
     * <p>
     * @return the key pressed by the user.
     */
    public String getNextMove(){
        System.out.println("""
                \tw - Up\s
                \ts - Down\s
                \td - Right\s
                \ta - Left""");
        System.out.print(">> ");

        return sn.next();
    }

    /**
     * Prints the most recent layout of the map.
     * <p>
     * @param mapLayout is the map to be shown.
     */
    public void updateMapView(String[][] mapLayout){
        int totalWidth = mapLayout.length + 2;
        String limitSign = "#";
        System.out.println("\t\t\t" + limitSign.repeat(totalWidth));
        for (String[] columns:
             mapLayout) {
            System.out.print("\t\t\t|");
            for (String column:
                 columns) {
                System.out.print(column);
            }
            System.out.println("|");
        }
        System.out.println("\t\t\t" + limitSign.repeat(totalWidth));
    }

    /**
     * Prints a win message for the player that has won the game.
     *<p>
     * @param name name of the player.
     * @param weapon weapon of the player.
     */
    public void gameWinMessage(String name, String weapon){
        String stringBuilder = "------- Congratulations -------\n" +
                "Player: " + name + "\n" +
                "Your " + weapon+ " has proven decisive in this battle";
        System.out.println(stringBuilder);
    }

    /**
     * Prints a warning when the player hits an obstacle.
     */
    public void obstacleHit(){
        System.out.println("You stepped over a pointy wall\n" + "player hit");
    }

    /**
     * Prints a message when the player has run out of lives.
     */
    public void playerDeadMessage(){
        System.out.println("GAME OVER - Your player is dead.");
    }

    /**
     * Prints a warning when the user choice is bad.
     */
    public void illegalChoice(){
        System.out.println("Bad option selected");
    }

    /**
     * Prints a warning when the user moves the player towards a wall.
     */
    public void illegalMovement(){
        System.out.println("That's a wall.");
    }

    /**
     * Shows a health bar of the player available lives.
     * <p>
     * @param playerHealthPoints the amount of health points the player has.
     * @param playerName the name of the player.
     */
    public void showPlayerHealthBar(String playerName, int playerHealthPoints){
        System.out.print(playerName +  " lives: ");
        System.out.println("[x]".repeat(playerHealthPoints) + "[]".repeat(5 - playerHealthPoints));
    }
}
