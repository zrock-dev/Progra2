package practice;

public class Queue<T extends Comparable<T>> {
    private Node<T> tail;
    private Node<T> head;

    public void enqueue(T data) {
        if (head == null) {
            head = new Node<>(tail, data);
        } else {
            searchForDuplicate(data);

            if (tail == null){
                tail = new Node<>(null, data);
                head.setAfter(tail);
            }else {
                Node<T> temp = new Node<>(null, data);
                tail.setAfter(temp);
                tail = temp;
            }
        }
    }

    public T dequeue() {
        if (head != null) {
            T data = head.getData();
            head = head.getAfter();
            return data;
        } else throw new NullPointerException("The queue is empty");
    }

    public boolean isEmpty(){
        return head == null;
    }

    // Dadas 2 colas, Intercalarlas en una sola cola, ej: [1,3,5] y [2,4,6] => [2,4,6,1,3,5].
    // Solo usando sus métodos add(queue) y remove(dequeue), sin enlazar nodos.

    public Queue<T> append(Queue<T> a, Queue<T> b){
        while (!a.isEmpty() && !b.isEmpty()){
            this.enqueue(a.dequeue());
            this.enqueue(b.dequeue());
        }
        return this;
    }

    private void searchForDuplicate(T control){
        for (Node<T> node = head; node.getAfter() != null; node = node.getAfter()){
            if (node.getData().compareTo(control) == 0){
                throw new IllegalArgumentException("The item: " + node.getData() + " is already in the queue");
            }
        }
    }


    @Override
    public String toString() {
        return head.toString();
    }
}

