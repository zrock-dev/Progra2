package practice;

public class Node<T> {
    private final T data;
    private Node<T> after;

    Node(Node<T> next, T data){
        this.data = data;
        this.after = next;
    }

    public T getData() {
        return data;
    }

    public Node<T> getAfter() {
        return after;
    }

    public void setAfter(Node<T> after) {
        this.after = after;
    }

    @Override
    public String toString() {
        return after + "<-" + data;
    }
}
