package rotate_array;

public class Rotator {

    /**
     * Rotates a given array with the elements inside the array rotated n spaces.
     * <p>
     * If n is greater than 0 it should rotate the array to the right. If n is
     * less than 0 it should rotate the array to the left. If n is 0, then it
     * should return the array unchanged.
     *
     * @param data Collection to be changed.
     * @param n number of times the rotation will be done.
     * @return A collection rotated n times.
     */
    public Object[] rotate(Object[] data, int n) {
        Object[] rotatedCollection = data;

        if (data.length != 0 && n != 0) {
            if (n > 0) {
                for (int index = 0; index < n ; index++) {
                    rotatedCollection = rearrangeArrayToTheRight(rotatedCollection);
                }
            }else {
                for (int index = 0; index < Math.abs(n) ; index++) {
                    rotatedCollection = rearrangeArrayToTheLeft(rotatedCollection);
                }
            }
        }
        return rotatedCollection;
    }

    /**
     * Changes a collection by moving the last item to the first position.
     * @param data Collection to be rotated.
     * @return Rotated collection.
     */
    private Object[] rearrangeArrayToTheRight(Object[] data){
        Object[] arrangedCollection = new Object[data.length];
        arrangedCollection[0] = data[data.length - 1];

        for (int index = 0; index < data.length - 1; index++) {
            arrangedCollection[index + 1] = data[index];
        }
        return arrangedCollection;
    }

    /**
     * Changes a collection by moving the first item to the last position.
     * @param data Collection to be rotated.
     * @return Rotated collection.
     */
    private Object[] rearrangeArrayToTheLeft(Object[] data){
        Object[] arrangedCollection = new Object[data.length];
        arrangedCollection[data.length - 1] = data[0];

        for (int index = 0; index < data.length - 1; index++) {
            arrangedCollection[index] = data[index + 1];
        }

        return arrangedCollection;
    }
}
