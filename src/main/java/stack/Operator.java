package stack;

import org.jetbrains.annotations.NotNull;
import stack.exceptions.EmptyException;
import stack.exceptions.IntegerMaxSizeException;
import stack.exceptions.MainStackEmpty;

/**
 * The Operator class resolves all the operations given in a stack.
 * it can cover basic arithmetic operations while considering the priority of each
 * sign, and also it can evaluate operations with association signs.
 */
public class Operator {
    private final Stack<Character> mainStack;
    private final Stack<Character> secondaryStack;
    private int answer;

    private char operationSign;
    private char firstChar;

    private String firstOperand;
    private String secondOperand;

    /**
     * This constructor initializes the class fields with the correct values.
     * <p>
     * @param mainStack is the stack to work with.
     */
    public Operator(Stack<Character> mainStack) {
        this.mainStack = new Analyzer().analyzeStack(mainStack);
        secondaryStack = new Stack<>();
        answer = -1;
        operationSign = ' ';
        firstChar = ' ';
    }

    /**
     * This method makes an operation between two numbers and a given sign.
     *<p>
     * @param exp1 This is the first operand.
     * @param sign This is the sign of the operation.
     * @param exp2 This is the second operand.
     * @return the final value of the operation.
     */
    private String makeOperation(String exp1, char sign, String exp2) {
        int num1 = Integer.parseInt(exp1);
        checkIntSize(num1);
        int num2 = Integer.parseInt(exp2);
        checkIntSize(num2);

        int resultOperation = switch (sign) {
            case ('^') -> (int) Math.pow(num1, num2);
            case ('+') -> num1 + num2;
            case ('*') -> num1 * num2;
            default -> throw new IllegalArgumentException(sign + "is not valid");
        };

        checkIntSize(resultOperation);
        return String.valueOf(resultOperation);
    }

    /**
     * This method evaluates if a number is exceeding the limit, if so it throws an exception.
     * <p>
     * @param number the number to be evaluated.
     */
    private void checkIntSize(int number) {
        if (number == Integer.MAX_VALUE) throw new IntegerMaxSizeException(number + " is up the limit.");
    }

    /**
     * This main method calls to calculate the tokens in the stack and then takes,
     * the final result.
     * <p>
     * @return the final result of the stack operation.
     */
    public int processStack() {
        try {
            calculate();
        } catch (MainStackEmpty ignored) {
            return answer;
        }
        return -1;
    }

    /**
     * This method iterates the stack to evaluate each token and
     * make an operation or call a method.
     * <p>
     * @throws MainStackEmpty when the final result is reached.
     */
    private void calculate() {
        if (!mainStack.isEmpty()) {
            firstChar = mainStack.peek() == '(' ? mainStack.pop() : ' ';

            firstOperand = getNumberFromStack();
            checkForAnswer(firstOperand);
            operationSign = isOperator(mainStack.peek(), false) ? mainStack.pop() : ' ';

            canFactorFirstOperand(firstOperand);
            evaluateGroupSigns(firstOperand);

            if (!mainStack.isEmpty() && mainStack.peek() == '(') {
                saveToSecondaryStack(firstOperand + operationSign);
                calculate();
            }

            secondOperand = getNumberFromStack();
            canFactorSecondOperand(firstOperand, secondOperand);

            evaluateOperands();
        }
    }

    /**
     * Verifies if the first operand can be factorized, if so then calls the method
     * to make the operation, then it saves the result in the same stack.
     * <p>
     * @param number the number to be evaluated.
     */
    private void canFactorFirstOperand(String number){
        if (operationSign == '!') {
            String result = firstChar == '(' ? (firstChar + factorize(number)) : factorize(number);
            saveToMainStack(result);
            calculate();
        }
    }

    /**
     * Verifies if the second operand can be factorized, if so then calls the method
     * to make the operation, then it saves the result in the same stack.
     * <p>
     * @param firstNumber first operand
     * @param secondNumber second operand
     * @throws MainStackEmpty when the final result is reached.
     */
    private void canFactorSecondOperand(String firstNumber, String secondNumber){
        if (!mainStack.isEmpty() && mainStack.peek() == '!') {
            mainStack.pop();
            saveToMainStack(firstNumber + operationSign + factorize(secondNumber));
            calculate();
        }
    }

    /**
     * This method evaluates the group signs in the stack.
     * @param firstOperand the first operand number.
     * @throws MainStackEmpty when the final result is reached.
     */
    private void evaluateGroupSigns(String firstOperand){
        if (firstChar != ' '){
            char secondChar = mainStack.peek() == ')' ? mainStack.pop() : ' ';
            if (secondChar != ' ') {
                saveToMainStack(firstOperand);
                fillStack(mainStack, secondaryStack);
                calculate();
            } else {
                saveToSecondaryStack(String.valueOf(firstChar));
            }
        }
    }

    /**
     * This method checks if the answer has been reached. If so then throws an exception.
     * <p>
     * @param firstOperand the first number of the operation.
     * @throws MainStackEmpty when the final result is reached.
     */
    private void checkForAnswer(String firstOperand){
        if(mainStack.isEmpty() && answer == -1){
            answer = Integer.parseInt(firstOperand);
            throw new MainStackEmpty();
        }
    }

    /**
     * This method builds a number into a string from the stack.
     * <p>
     * @return a number from the stack.
     */
    private String getNumberFromStack() {
        String number = "";
        while (!mainStack.isEmpty()) {
            if (!isOperator(mainStack.peek(), true)) {
                number += mainStack.pop();
            } else {
                break;
            }
        }
        if (number.equals("")) throw new EmptyException("Can't find a number." + mainStack);
        return number;
    }

    /**
     * This method evaluates if the last operand has been reached based in the state
     * of the mainStack.
     * <p>
     * @throws MainStackEmpty when the final result is reached.
     */
    private void evaluateOperands() throws MainStackEmpty {
        if (!mainStack.isEmpty()) {
            calculateOperands();
        } else {
            lastOperandsProtocol();
        }
        calculate();
    }

    /**
     * This method evaluate if the operation between two operands can be made,
     * it takes care of the signs' priority.
     * <p>
     * @throws MainStackEmpty when the final result is reached.
     */
    private void calculateOperands() {
        char nextSign = mainStack.peek();

        if (nextSign == ')') {
            saveToSecondaryStack(makeOperation(firstOperand, operationSign, secondOperand) + mainStack.pop());
            fillStack(mainStack, secondaryStack);
            calculate();

        } else if (compareSigns(operationSign, nextSign)) {
            saveToMainStack(makeOperation(firstOperand, operationSign, secondOperand));
        } else {
            saveToMainStack(secondOperand);
            saveToSecondaryStack(firstOperand + operationSign);
        }
    }

    /**
     * This method verifies if the secondaryStack is empty in order to be able to
     * save the final answer in the answer class field.
     * <p>
     * @throws MainStackEmpty when the final result is reached.
     */
    private void lastOperandsProtocol() {
        String lastOperation = makeOperation(firstOperand, operationSign, secondOperand);

        if (answer == -1 && secondaryStack.isEmpty()) {
            answer = Integer.parseInt(lastOperation);
            throw new MainStackEmpty();
        } else {
            saveToSecondaryStack(lastOperation);
            fillStack(mainStack, secondaryStack);
        }
    }

    /**
     * This method fills a given stack with the data of the other given stack.
     * <p>
     * @param toFill The stack to be fill with.
     * @param fromStack The stack to take the data from.
     */
    private void fillStack(Stack<Character> toFill, @NotNull Stack<Character> fromStack) {
        while (!fromStack.isEmpty()) {
            toFill.push(fromStack.pop());
        }
    }

    /**
     * This method factorizes a given expression.
     * <p>
     * @param expression The number to be factorized.
     * @return The result as String.
     */
    private @NotNull String factorize(String expression) {
        int number = Integer.parseInt(expression);
        long factorial = 1;
        for (int i = 1; i <= number; i++) {
            factorial *= i;
        }
        return String.valueOf(factorial);
    }

    /**
     * This method saves a number into the main stack.
     * <p>
     * @param number The number to save into the main stack.
     */
    private void saveToMainStack(@NotNull String number){
        for (int i = number.length() - 1; i >= 0; i--) {
            mainStack.push(number.charAt(i));
        }
    }

    /**
     * This method saves a number into the secondaryStack
     * <p>
     * @param number The number to save into the secondary stack.
     */
    private void saveToSecondaryStack( @NotNull String number){
        for (int i = 0; i < number.length(); i++) {
            secondaryStack.push(number.charAt(i));
        }
    }

    /**
     * This method verifies if a given expression is an operand.
     * <p>
     * @param expression the expression to be evaluated.
     * @param includeGroupSigns This flag adds the group signs to the range of evaluation.
     * @return a boolean result of the evaluation, true if the expression is an operand.
     */
    private boolean isOperator(char expression, boolean includeGroupSigns){
        if (includeGroupSigns) {
            return switch (expression) {
                case '(', ')', '!', '^', '+', '*' -> true;
                default -> false;
            };
        }else {
            return switch (expression) {
                case '!', '^', '+', '*' -> true;
                default -> false;
            };
        }
    }

    /**
     * This method compares two signs to determine which one has a higher priority.
     *<p>
     * @param sign the first sign to evaluate, it's located between the first and second operand.
     * @param peekSign the second sign to evaluate, located next to the second operand.
     * @return a boolean evaluation, true if the sign has the higher priority.
     */
    private boolean compareSigns(char sign, char peekSign){

        if (sign == peekSign) return true;
        else if (sign == '^' && peekSign == '+') return true;
        else if (sign == '+' && peekSign == '^') return false;

        else if (sign == '^' && peekSign == '*') return true;
        else if (sign == '*' && peekSign == '^') return false;

        else if (sign == '+' && peekSign == '*') return true;
        else if (sign == '*' && peekSign == '+') return false;

        throw new IllegalArgumentException(sign+ " vs "+ peekSign);
    }
}
