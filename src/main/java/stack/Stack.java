package stack;

class Node<T> {
    private final T data;
    private Node<T> next;

    public Node(T data) { this.data = data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public T getData() { return data;}

    public String toString() { return "-> " + data + " " + next; }
}

public class Stack <T extends Comparable<T>>{
    private Node<T> head;

    public void push(T data){
        if (head == null){
            head = new Node<>(data);
            head.setNext(null);
        }else {
            Node<T> node = new Node<>(data);
            node.setNext(head);
            head = node;
        }
    }

    public T pop(){
        if (head != null){
            T top = head.getData();
            head = head.getNext();
            return top;
        }else throw new IllegalStateException();
    }

    public T peek(){
        if(head != null){
            return head.getData();
        }else throw new IllegalStateException();
    }

    public boolean isEmpty(){
        return head == null;
    }

    @Override
    public String toString() {
        return head.toString();
    }
}
