package stack;

import stack.exceptions.BadArithmeticSyntaxException;
import stack.exceptions.EmptyException;
import stack.exceptions.InvalidTokenException;
import stack.exceptions.UnpairGroupSigns;

/**
 * The Analyzer class, works as a verifier or checker to the elements in a stack,
 * throwing exceptions for different cases taking in care the valid syntax for the
 * operations.
 */
public class Analyzer {
    private char value;
    private char nextValue;
    private int openParenthesisCount = 0;
    private int closedParenthesisCount = 0;

    /**
     * analyzeStack is the main method, it loops a given stack applying different
     * filters to it.
     *
     * @param stack is the stack to review or analyze.
     * @return a reviewed stack with valid tokens.
     */
    public Stack<Character> analyzeStack(Stack<Character> stack) {
        checkStackIsNotEmpty(stack);
        stack = revertStack(stack);
        Stack<Character> reviewedStack = new Stack<>();

        while (!stack.isEmpty()) {
            value = stack.pop();
            validateChar(value);
            countParenthesis();

            if(!stack.isEmpty()){
                nextValue = stack.peek();
                checkForEmptyGroup();
                validateOperatorNextToOperator();
                checkNumberNextToParentheses();
            }
            reviewedStack.push(value);
        }
        evaluateParenthesesCount();
        return revertStack(reviewedStack);
    }

    /**
     * Checks if a given stack is empty.
     *
     * @param stack the stack to verify.
     * @throws NullPointerException When the stack is empty.
     */
    private void checkStackIsNotEmpty(Stack<Character> stack){
        if (stack.isEmpty()) throw new NullPointerException("Stack can't be empty");
    }

    /**
     * Checks the situation when two parenthesis have nothing between them.
     * for example: ()
     * <p>
     * @throws EmptyException When the condition is reached.
     */
    private void checkForEmptyGroup(){
        if (value == '(' && nextValue == ')') throw new EmptyException(value + (nextValue + " can't be empty"));
    }

    /**
     * Checks if the current value and the next value can be concatenated,
     * for example a: !+ or ++
     * <p>
     * @throws BadArithmeticSyntaxException When the syntax of the operation is illogical.
     */
    private void validateOperatorNextToOperator(){
        if (!Character.isDigit(value) && !Character.isDigit(nextValue)){
            if (value != '!' && isOperator(value) && nextValue != '(' )
                throw new BadArithmeticSyntaxException(value + " can't be next to " + nextValue);
        }
    }

    /**
     * Checks the situation when a number is next to a parentheses,
     * which isn't valid.
     * for example: 3(
     * <p>
     * @throws BadArithmeticSyntaxException When the syntax of the operation is illogical.
     */
    private void checkNumberNextToParentheses(){
        if(value == ')' && Character.isDigit(nextValue)){
            throw new BadArithmeticSyntaxException(value + " can't be next to " + nextValue);
        }
    }

    /**
     * Keeps account of the number of repetitions of two signs: (, ).
     *
     */
    private void countParenthesis(){
        switch (value) {
            case '(' -> openParenthesisCount++;
            case ')' -> closedParenthesisCount++;
        }
    }

    /**
     * Evaluates the count to verify if the difference between the number of repetitions for
     * the group signs: (, ) is equal to zero.
     * <p>
     * @throws UnpairGroupSigns If the difference isn't equal to zero, then the stack of operations
     * isn't valid.
     */
    private void evaluateParenthesesCount(){
        int difference = openParenthesisCount - closedParenthesisCount;
        if (difference != 0) throw new UnpairGroupSigns(String.valueOf(difference));
    }

    /**
     * Reverts a stack, by reverting the start with the end.
     * <p>
     * @param stack the stack to be reverted.
     * @return the new reverted stack.
     */
    private Stack<Character> revertStack(Stack<Character> stack) {
        Stack<Character> reverse = new Stack<>();
        while (!stack.isEmpty()) {
            reverse.push(stack.pop());
        }
        return reverse;
    }

    /**
     * In the stack there only can be numbers and signs, this method ensures that.
     * By the condition: if the character isn't a number, then it only can be an operator;
     * as a result other characters are filtered out.
     * <p>
     * @param value the character to be evaluated.
     * @throws InvalidTokenException if the character isn't a valid operator sign.
     */
    private void validateChar(char value) {
        if (!Character.isDigit(value)) {
            switch (value) {
                case '(', ')', '!', '^', '+', '*': break;
                default : throw new InvalidTokenException(value + " is not a valid token");
            }
        }
    }

    /**
     * This method compares a given character against basic arithmetic operations
     * such as !, ^, +, or *.
     * <p>
     * @param expression the expression to check.
     * @return true if the character is a signs, and false if it's something else.
     */
    private boolean isOperator(char expression) {
        return switch (expression) {
            case '!', '^', '+', '*' -> true;
            default -> false;
        };
    }
}
