package stack.exceptions;

/**
 * IntegerMaxSizeException is thrown whenever an number reaches the maximum value
 * for an integer.
 */
public class IntegerMaxSizeException extends RuntimeException{
    public IntegerMaxSizeException(String message) {
        super(message);
    }

    public IntegerMaxSizeException(){
        super("A number inside the stack has reached the maximum value");
    }
}
