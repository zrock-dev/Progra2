package stack.exceptions;

/**
 * The BadArithmeticSyntaxException is thrown when a operation has a bad syntax.
 * <p>
 * For example an operator next to an operator like: ++
 * Or if a number is next to a parenthesis like: 3(
 */
public class BadArithmeticSyntaxException extends RuntimeException{
    public BadArithmeticSyntaxException() {
        super("Looks like you have wrote an operation incorrectly.");
    }

    public BadArithmeticSyntaxException(String message) {
        super(message);
    }
}
