package stack.exceptions;

/**
 * The UnpairGroupSigns exception is thrown when the number of open and closed
 * group signs in the stack aren't equal.
 */

public class UnpairGroupSigns extends RuntimeException{
    public UnpairGroupSigns(String message) {
        super(message);
    }

    public UnpairGroupSigns(){
        super("The number of open and close group signs is not pair");
    }
}
