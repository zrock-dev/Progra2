package stack.exceptions;

/**
 * The exception MainStackEmpty is thrown when the main stack is empty and the final result
 * has been reached.
 */
public class MainStackEmpty extends RuntimeException {
    public MainStackEmpty(){
        super("The main stack under evaluation is empty");
    }
}
