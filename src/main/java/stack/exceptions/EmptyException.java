package stack.exceptions;

/**
 * EmptyException is thrown whenever an empty string or empty group is detected.
 * <p>
 */
public class EmptyException extends RuntimeException{
    public EmptyException(String message) {
        super(message);
    }

    public EmptyException(){
        super("The evaluated field is empty");
    }
}
