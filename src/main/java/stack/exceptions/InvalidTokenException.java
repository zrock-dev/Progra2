package stack.exceptions;

/**
 * InvalidTokenException is thrown whenever a token in a stack isn't valid.
 * <p>
 * For example a blank space, or a symbol that isn't an operator: !, ^, *, +
 */

public class InvalidTokenException extends RuntimeException {
    public InvalidTokenException(String message) {
        super(message);
    }

    public InvalidTokenException() {
        super("Looks like an item in the stack isn't valid");
    }
}
