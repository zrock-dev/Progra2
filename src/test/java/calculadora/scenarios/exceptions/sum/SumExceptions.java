package calculadora.scenarios.exceptions.sum;

import calculadora.AbstractCalc;
import calculadora.MockCalc;
import calculadora.exceptions.InvalidTokenException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SumExceptions {
    @Test
    public void validSumTest() {
        AbstractCalc calc = new MockCalc();
        int actual = assertDoesNotThrow(() -> calc.calc("1+1"));
        int expected = 1;

        assertEquals(expected,actual);
    }

    @Test
    public void invalidTokenSumTest() {
        AbstractCalc calc = new MockCalc();
//        InvalidTokenException ex = assertThrows(InvalidTokenException.class, () -> calc.calc("1-1"));
//        String expected = "-, is not valid token, at column 1";
//        String actual = ex.getMessage();
//
//        assertEquals(expected,actual);
    }

    @Test
    public void malformedTokenSumTest() {
        AbstractCalc calc = new MockCalc();
        //Should implement Exceptions needed
//        InvalidTokenException ex = assertThrows(MalformedException.class, () -> calc.calc("1++1"));
//
//        String expected = "++, is not expected operation usage, at column 1";
//        String actual = ex.getMessage();
//
//        assertEquals(expected,actual);
    }

    @Test
    public void incompledOperanExpectedTokenSumTest() {
        AbstractCalc calc = new MockCalc();
        //Should implement Exceptions needed
//        InvalidTokenException ex = assertThrows(public void incompledOperanExpectedException.class, () -> calc.calc("1+1+"));
//
//        String expected = "+ operation, needs operation an operand, at column 5";
//        String actual = ex.getMessage();
//
//        assertEquals(expected,actual);
    }

    @Test
    public void missingCloseRighBracketTokenSumTest() {
        AbstractCalc calc = new MockCalc();
        //Should implement Exceptions needed
//        InvalidTokenException ex = assertThrows(public void MissingCloseRighBracketException.class, () -> calc.calc("(1+1+1"));
//
//        String expected = "+ operation, missing right bracket, at column 5";
//        String actual = ex.getMessage();
//
//        assertEquals(expected,actual);
    }

    @Test
    public void missingCloseLeftBracketTokenSumTest() {
        AbstractCalc calc = new MockCalc();
        //Should implement Exceptions needed
//        InvalidTokenException ex = assertThrows(public void MissingCloseRighBracketException.class, () -> calc.calc("1+1+1"));
//
//        String expected = "+ operation, missing left bracket, at column 0";
//        String actual = ex.getMessage();
//
//        assertEquals(expected,actual);
    }

    @Test
    public void nestedBracketTokenSumTest() {
        AbstractCalc calc = new MockCalc();
        //Should implement Exceptions needed
//        InvalidTokenException ex = assertThrows(public void MissingCloseRighBracketException.class, () -> calc.calc("(1+1) + (1 + (1 + (1+1))"));
//
//        String expected = "+ operation, missing left bracket, at column 26";
//        String actual = ex.getMessage();
//
//        assertEquals(expected,actual);
    }
}
