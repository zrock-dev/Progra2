package nodes;

import org.junit.Assert;

import static org.junit.jupiter.api.Assertions.assertNull;

public class NodeHelper {
    public static Node push(final Node head, final int data) {
        return new Node( data, head );
    }

    public static Node build(int[] arr) {
        Node node = null;
        for ( int i = arr.length - 1; i > -1; i-- ) {
            node = push( node, arr[i] );
        }
        return node;
    }

    public static void assertEquals(Node a, Node b) {
        while ( a != null && b != null ) {
            Assert.assertEquals( a.data, b.data );
            a = a.next;
            b = b.next;
        }
        assertNull( a );
        assertNull( b );
    }
}
