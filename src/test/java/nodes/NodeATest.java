package nodes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NodeATest {

    @Test
    public void test2() {
        NodeA n = new NodeA();
        n.data = 1337;
        n.next = new NodeA();
        n.next.data = 42;
        n.next.next = new NodeA();
        n.next.next.data = 23;
        try{
            assertEquals(NodeA.getNth(n, 0), 1337);
            assertEquals(NodeA.getNth(n, 1), 42);
            assertEquals(NodeA.getNth(n, 2), 23);
        }catch(Exception e){
            fail();
        }
    }

    @Test
    public void testNull() {
        try{
            NodeA.getNth(null, 0);
            fail();
        }catch(Exception e){
            assertTrue(true);
        }
    }


    @Test
    public void testWrongIdx() {
        try{
            NodeA.getNth(new NodeA(), 1);
            fail();
        }catch(Exception e){
            assertTrue(true);
        }
    }
}