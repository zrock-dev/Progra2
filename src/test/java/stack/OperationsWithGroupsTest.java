package stack;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static stack.OperatorsMixTest.getTokens;

public class OperationsWithGroupsTest {

    @Test
    void simpleTest(){
        String expression = "(1*2)+3";
        Operator operator = new Operator(getTokens(expression));
        assertEquals(5, operator.processStack());
    }

    @Test
    void multiGroup(){
        String expression = "12+23+(2^(1!+2!)*5)"; // 5
        Operator operator = new Operator(getTokens(expression));
        assertEquals(75, operator.processStack());
    }

    @Test
    void factor(){
        String expression = "(3+2)!";
        Operator operator = new Operator(getTokens(expression));
        assertEquals(120, operator.processStack());

        operator = new Operator(getTokens("3!"));
        assertEquals(6, operator.processStack());
    }

    @Test
    void factorAndOperatorsMix(){
        String expression = "3^3!+(5*2)";
        Operator operator = new Operator(getTokens(expression));
        assertEquals(739, operator.processStack());
    }

    @Test
    void factorAndGroup(){
        String expression = "(3!)^2";
        Operator operator = new Operator(getTokens(expression));
        assertEquals(36, operator.processStack());
    }

}
