package stack;

import org.junit.jupiter.api.Test;
import java.util.Random;
import static org.junit.jupiter.api.Assertions.*;

public class OperatorsMixTest {
    char[] operators = new char[]{'^', '+', '*'};

    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        return tokens;
    }

    @Test
    void simpleOperation(){
        String expression = "1+1+1+1+1";
        Operator operator = new Operator(getTokens(expression));
        assertEquals(5, operator.processStack());
    }

    @Test
    void multipleOperators(){
        String expression = "1*3+5*2^5+4"; // 1 * 8 * 32 + 4 = 8 * 36
        Operator operator = new Operator(getTokens(expression));
        assertEquals(8*36, operator.processStack());
    }

    @Test
    void operationsWithFactorization(){
        String expression = "3!+32*1!^4!*34";
        Operator operator = new Operator(getTokens(expression));
        assertEquals(1292, operator.processStack());
    }

    @Test
    void doubleFactor(){
        String expression = "(3!)!";
        Operator operator = new Operator(getTokens(expression));
        assertEquals(720, operator.processStack());
    }
}
