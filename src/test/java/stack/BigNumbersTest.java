package stack;

import org.junit.jupiter.api.Test;
import stack.exceptions.IntegerMaxSizeException;
import static org.junit.jupiter.api.Assertions.*;
import java.util.Random;

import static stack.OperatorsMixTest.getTokens;

public class BigNumbersTest {
    Random random = new Random();
    private int getBigNumber(){
        return random.nextInt(2147480000, 2147483646);
    }
    @Test
    void bigNumbers(){
        String stringBuilder = getBigNumber() +
                "+" +
                getBigNumber() +
                "+" +
                getBigNumber() +
                "^" +
                getBigNumber() +
                "+" +
                getBigNumber() +
                "+" +
                getBigNumber();

        Operator operator = new Operator(getTokens(stringBuilder));
        try {
            operator.processStack();
        } catch (IntegerMaxSizeException e) {
            assertSame(IntegerMaxSizeException.class, e.getClass());
        }
    }
}
