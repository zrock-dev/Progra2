package stack;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class StackTest {
    Stack<Integer> stack;
    int[] arrayExpected;

    @BeforeEach
    void init(){
        stack = new Stack<>();
        arrayExpected = new int[10];
        for (int i = 0; i < arrayExpected.length; i++) {
            stack.push(i);
            arrayExpected[i] = 9-i;
        }
    }

    @Test
    @DisplayName("Push to stack")
    void push(){
        Stack<Integer> stack = new Stack<>();
        String expected = "";
        for (int i = 0; i < 10; i++){
            stack.push(i);
            expected += "-> " + (9-i) + " ";
        }
        expected += "null";
        assertEquals(expected, stack.toString());
    }

    @Test
    @DisplayName("Pop from stack")
    void pop(){
        for (int number: arrayExpected) {
            assertEquals(number, stack.pop());
        }
    }

    @Test
    @DisplayName("Peek stack")
    void peek(){
        assertEquals(arrayExpected[0], stack.peek());
    }


    @Test
    @DisplayName("Stack head is null")
    void headNull(){
        Stack<Integer> nullStack = new Stack<>();
        assertThrows(IllegalStateException.class, nullStack::pop);
        assertThrows(IllegalStateException.class, nullStack::peek);
    }
}
