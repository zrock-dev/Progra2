package stack;

import org.junit.jupiter.api.Test;
import stack.exceptions.BadArithmeticSyntaxException;
import stack.exceptions.EmptyException;
import stack.exceptions.InvalidTokenException;
import stack.exceptions.UnpairGroupSigns;
import static org.junit.jupiter.api.Assertions.*;
import static stack.OperatorsMixTest.getTokens;

public class BadTokensTest {
    Analyzer analyzer = new Analyzer();

    @Test
    void signNextToSign(){
        String expression = "3+2+1+^3";
        try {
            analyzer.analyzeStack(getTokens(expression));
        }catch (BadArithmeticSyntaxException ex){
            assertEquals("+ can't be next to ^", ex.getMessage());
        }

        expression = "3!+!";
        try {
            analyzer.analyzeStack(getTokens(expression));
        }catch (BadArithmeticSyntaxException ex){
            assertEquals("+ can't be next to !", ex.getMessage());
        }
    }

    @Test
    void invalidCharacters(){
        String expression = "32 + 32";
        try {
            analyzer.analyzeStack(getTokens(expression));
        }catch (InvalidTokenException ex){
            assertEquals("  is not a valid token", ex.getMessage());
        }

        expression = "44-2";
        try {
            analyzer.analyzeStack(getTokens(expression));
        }catch (InvalidTokenException ex){
            assertEquals("- is not a valid token", ex.getMessage());
        }

        expression = "40+32*32;+1";
        try {
            analyzer.analyzeStack(getTokens(expression));
        }catch (InvalidTokenException ex){
            assertEquals("; is not a valid token", ex.getMessage());
        }
    }

    @Test
    void groupSigns(){
        String expression = "(1*2+(";

        try {
            analyzer.analyzeStack(getTokens(expression));
        }catch (UnpairGroupSigns ex){
            assertEquals("2", ex.getMessage());
        }

        expression = "(1)+2)";
        try {
            Analyzer analyzer1 = new Analyzer();
            analyzer1.analyzeStack(getTokens(expression));
        }catch (UnpairGroupSigns ex){
            assertEquals("-1", ex.getMessage());
        }
    }

    @Test
    void groupSignNextToSign(){
        String expression = "((1)+)";
        try {
            analyzer.analyzeStack(getTokens(expression));
        }catch (BadArithmeticSyntaxException ex){
            assertEquals("+ can't be next to )", ex.getMessage());
        }
    }

    @Test
    void emptyGroup(){
        String expression = "32+32*(44+1*())";
        try {
            analyzer.analyzeStack(getTokens(expression));
        }catch (EmptyException ex){
           assertEquals("() can't be empty", ex.getMessage());
        }
    }

    @Test
    void correctAnalysis(){
        String expression = "3^3!+(5*2)";
        Stack<Character> stack = analyzer.analyzeStack(getTokens(expression));
        assertEquals("-> 3 -> ^ -> 3 -> ! -> + -> ( -> 5 -> * -> 2 -> ) null", stack.toString());
    }
}
