package circular_linked_list;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class CircularLinkedListIntTest {
    int ARRAY_SIZE = 1000;

    /*
     * En todos los casos su clase Nodo debe ser externo a la lista y su campo `T data` privado sin `setData`.
     * Para las opciones: A, B, D, E, G, H, las pruebas unitarias deben enfocarse en sus métodos `sort`,
     * y deben contemplar varios casos.
     * Todas las pruebas unitarias con ítems, Integer y otra con String.
     */

    @Test
    void add(){
        CircularLinkedList<Integer> list = new CircularLinkedList<>();
        for (int i = 0; i < 100; i++) {
            list.add(i);
            assertEquals(i, list.get(i));
        }

        assertNull(list.get(-1));
        assertEquals(0, list.get(100));
    }

    @Test
    void remove(){
        CircularLinkedList<Integer> list = new CircularLinkedList<>();
        list.add(20);
        list.add(21);
        list.add(22);
        list.add(23);
        list.add(24);

        assertTrue(list.remove(20));
        assertTrue(list.remove(24));

        assertEquals(21, list.get(0));
        assertEquals(23, list.get(2));
    }

    @Test
    void selectionSort(){
        CircularLinkedList<Integer> list = new CircularLinkedList<>();
        int[] numbers = makeUnsortedList(ARRAY_SIZE);
        for (int n: numbers) {
            list.add(n);
        }
        Instant before = Instant.now();
        list.selectionSort();
        Instant after = Instant.now();
        sort(numbers);
        for (int i = 0; i < numbers.length; i++) {
            assertEquals(numbers[i], list.get(i));
        }
        System.out.println("|Selection Sort| Elapsed Time: "+ Duration.between(before, after).toMillis() + "ms");

    }

    @Test
    void bubbleSort(){
        CircularLinkedList<Integer> list = new CircularLinkedList<>();
        int[] numbers = makeUnsortedList(ARRAY_SIZE);
        for (int n: numbers) {
            list.add(n);
        }

        Instant before = Instant.now();
        list.bubbleSort();
        Instant after = Instant.now();
        sort(numbers);
        for (int i = 0; i < numbers.length; i++) {
            assertEquals(numbers[i], list.get(i));
        }
        System.out.println("|Bubble Sort| Elapsed Time: "+ Duration.between(before, after).toMillis() + "ms");
    }

    private void swap(int[] array, int x, int y){
        int aux = array[x];
        array[x] = array[y];
        array[y] = aux;
    }

    private int[] makeUnsortedList(int size){
        Random random = new Random();
        int[] array =  new int[size];
        int randomNumber;
        for (int i = 0; i < size; i++) {
           do {
               randomNumber = random.nextInt(1, size * 2);
               if (array[array.length - 1] != 0){
                   break;
               }
           }while (is_repeated(randomNumber, array));
            array[i] = randomNumber;
        }
        return array;
    }

    private boolean is_repeated(int number, int[] array){
        for (int num:
             array) {
            if (num == number) {
                return true;
            }
        }
        return false;
    }

    private void sort(int[] array){
        for (int i = 0; i < array.length; i++) {
            int index = i;
            int minNumber = array[i];
            for (int j = i; j < array.length; j++) {
                if (minNumber > array[j]){
                    minNumber = array[j];
                    index = j;
                }
            }
            swap(array, i, index);
        }
    }
}