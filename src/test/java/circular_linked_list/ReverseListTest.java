package circular_linked_list;

import org.junit.jupiter.api.Test;
import java.util.Random;
import static org.junit.jupiter.api.Assertions.*;

public class ReverseListTest {
    @Test
    void reverseOddList(){
        CircularLinkedList<Integer> list = new CircularLinkedList<>();
        int[] array = makeList(17);
        for (int i = 0; i < array.length; i++) {
            list.add(array[i]);
        }
        System.out.println(list);
        reverse(array);
        list.reverse();
        for (int i = 0; i < array.length; i++) {
            assertEquals(array[i], list.get(i));
        }
        System.out.println(list);
    }

    @Test
    void reverseEvenList(){
        CircularLinkedList<Integer> list = new CircularLinkedList<>();
        int[] array = makeList(24);
        for (int i = 0; i < array.length; i++) {
            list.add(array[i]);
        }
        System.out.println(list);
        reverse(array);
        list.reverse();
        for (int i = 0; i < array.length; i++) {
            assertEquals(array[i], list.get(i));
        }
        System.out.println(list);
    }

    private void reverse(int[] array){
        for (int i = 0; i < array.length / 2; i++) {
            swap(array, i, array.length - 1 - i);
        }
    }

    private void swap(int[] array, int x, int y){
        int aux = array[x];
        array[x] = array[y];
        array[y] = aux;
    }

    private int[] makeList(int size){
        Random random = new Random();
        int[] array =  new int[size];
        int randomNumber;
        for (int i = 0; i < size; i++) {
            do {
                randomNumber = random.nextInt(1, size * 2);
                if (array[array.length - 1] != 0){
                    break;
                }
            }while (is_repeated(randomNumber, array));
            array[i] = randomNumber;
        }
        return array;
    }

    private boolean is_repeated(int number, int[] array){
        for (int num:
                array) {
            if (num == number) {
                return true;
            }
        }
        return false;
    }
}
