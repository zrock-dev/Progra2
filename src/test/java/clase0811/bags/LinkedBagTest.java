package clase0811.bags;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedBagTest {
    Bag<Integer> list = new LinkedBag<>();

    @Test
    void happyTest(){
        Bag<Integer> controlList = new LinkedBag<>();
        controlList.add(5); controlList.add(4); controlList.add(3); controlList.add(2); controlList.add(1);

        list.add(5); list.add(3); list.add(4); list.add(2); list.add(1);
        list.selectionSort();
        assertEquals(controlList.toString(), list.toString());
    }

    @Test
    void swapNumbersByIndex(){
        Bag<Integer> controlList = new LinkedBag<>();
        controlList.add(1); controlList.add(2); controlList.add(3); controlList.add(5); controlList.add(4);

        list.add(1); list.add(2); list.add(3); list.add(4); list.add(5);
        list.xchange(1, 0);

        assertEquals(controlList.toString(), list.toString());
    }

    @Test
    void swapNumbersByIndex1(){
        Bag<Integer> controlList = new LinkedBag<>();
        controlList.add(5); controlList.add(2); controlList.add(3); controlList.add(4); controlList.add(1);

        list.add(1); list.add(2); list.add(3); list.add(4); list.add(5);
        list.xchange(4, 0);

        assertEquals(controlList.toString(), list.toString());
    }

    @Test
    void swapNumbersByXMoreThanZero(){
        Bag<Integer> controlList = new LinkedBag<>();
        controlList.add(2); controlList.add(1); controlList.add(3); controlList.add(4); controlList.add(5);

        list.add(1); list.add(2); list.add(3); list.add(4); list.add(5);
        list.xchange(4, 3);

        assertEquals(controlList.toString(), list.toString());
    }

}