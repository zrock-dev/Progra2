package game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameMapTest {
    @Test
    void tenByTenMap(){
        GameMap map = new GameMap();

        assertEquals(10, map.size());
    }

}