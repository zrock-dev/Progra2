package game.player;

import game.ConsoleMessages;
import game.GameMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    PlayerManager playerManager;
    String playerWeapon;
    String playerName;

    @BeforeEach
    void initPlayer(){
        playerName = "Jack the killer";
        playerWeapon = "Knife";
        playerManager = new PlayerManager(new Player(playerName, playerWeapon), new GameMap(), new ConsoleMessages());
    }

    @Test
    void playerGotHit(){
        Player player = playerManager.getPlayer();
        int playerLives = player.getHealthPoints();

        player.hitPlayer();

        assertEquals(playerLives - 1, player.getHealthPoints());
        assertEquals(playerName, player.getName());
        assertEquals(playerWeapon, player.getWeapon());
    }

    @Test
    void playerIsDead(){
        Player player = playerManager.getPlayer();

        player.hitPlayer();
        player.hitPlayer();
        player.hitPlayer();
        player.hitPlayer();
        player.hitPlayer();

        PlayerManager playerManager = new PlayerManager(player, new GameMap(), new ConsoleMessages());
        assertFalse(playerManager.isPlayerAlive());
    }

}