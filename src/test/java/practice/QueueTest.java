package practice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class QueueTest {
    
    @Test
    void dequeue(){
        Queue<String> queue = new Queue<>();

        for (int i = 0; i < 10; i++) {
            queue.enqueue(""+i);
        }

        for (int i = 0; i < 10; i++) {
            assertEquals(""+i, queue.dequeue());
        }
    }

    @Test
    void mixTest(){
        Queue<Integer> integerQueue = new Queue<>();
        integerQueue.enqueue(1);
        integerQueue.enqueue(2);
        integerQueue.enqueue(3);
        integerQueue.enqueue(4);

        assertEquals(1, integerQueue.dequeue());
        assertEquals(2, integerQueue.dequeue());
        assertEquals(3, integerQueue.dequeue());
        assertEquals(4, integerQueue.dequeue());

        integerQueue.enqueue(5);
        assertEquals(5, integerQueue.dequeue());

    }

    // Dadas 2 colas, Intercalarlas en una sola cola, ej: [1,3,5] y [2,4,6] => [2,4,6,1,3,5].
    // Solo usando sus métodos add(queue) y remove(dequeue), sin enlazar nodos.
    @Test
    void mergeQueues(){
        int[] control = new int[]{1, 2, 3, 4, 5, 6};

        Queue<Integer> queueA = new Queue<>();
        Queue<Integer> queueB = new Queue<>();

        queueA.enqueue(1);
        queueA.enqueue(3);
        queueA.enqueue(5);

        queueB.enqueue(2);
        queueB.enqueue(4);
        queueB.enqueue(6);

        Queue<Integer> merged = new Queue<>();
        merged.append(queueA, queueB);

        for (int number:
             control) {
            assertEquals(number, merged.dequeue());
        }
    }

    @Test
    void duplicatedItems(){
        Queue<Integer> integerQueue = new Queue<>();
        try {
            integerQueue.enqueue(2);
            integerQueue.enqueue(1);
            integerQueue.enqueue(3);
            integerQueue.enqueue(3);
        }catch (IllegalArgumentException exception){
            assertEquals("The item: 3 is already in the queue", exception.getMessage());
        }
    }

}