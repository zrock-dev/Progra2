package list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Iterator;
import static org.junit.jupiter.api.Assertions.*;

class JUArrayListTest {
    JUArrayList list;

    @BeforeEach
    void setUp() {
        list = new JUArrayList();
    }

    @Test
    void add() {

        for (int index = 0; index < 30; index++) {
            assertTrue(list.add(index));
        }
    }

    @Test
    void addIndex(){
        list.add(50); // 0
        list.add(51); // 1
        list.add(52); // 2
        list.add(53); // 3

        assertEquals(52, list.get(2));
        list.add(2, 32);

        assertEquals(32, list.get(2));
        assertEquals(53, list.get(4));
    }

    @Test
    void remove() {
        int itemsInArray = 11;
        for (int index = 0; index <= itemsInArray; index++) {
            list.add(index);
        }

        for (int index = itemsInArray; index >= 0; index--) {
            assertEquals(index, list.remove(index));
        }
    }

    @Test
    void removeObject(){
        list.add(32);
        Object number = list.get(0);
        assertTrue(list.remove(number));
    }

    @Test
    void size() {
        list.add(3);
        list.add(3);
        list.add(3);
        list.add(3);

        assertEquals(4, list.size());
    }

    @Test
    void isEmpty() {
        int itemsInArray = 11;
        for (int index = 0; index <= itemsInArray; index++) {
            list.add(index);
        }

        for (int index = itemsInArray; index >= 0; index--) {
            list.remove(index);
        }

        assertTrue(list.isEmpty());
    }

    @Test
    void contains() {
        list.add(32);
        list.add(33);
        list.add(34);
        list.add(35);
        assertFalse(list.contains(0));
        assertTrue(list.contains(33));
    }

    @Test
    void iterator() {
        Iterator<Integer> iterator = list.iterator();
        assertFalse(iterator.hasNext());
        list.add(3);
        assertTrue(iterator.hasNext());
        assertEquals(3, iterator.next());
    }

    @Test
    void toArray() {
        list.add(3);
        list.add(4);

        Object[] comparator = new Object[]{3, 4};

        for (int index = 0; index < comparator.length; index++) {
            assertEquals(comparator[index], list.toArray()[index]);
        }

    }

    @Test
    void testToArray() {
    }

    @Test
    void clear() {
        list.clear();
        assertTrue(list.isEmpty());
    }

    @Test
    void get() {
        list.add(21);
        assertEquals(21, list.get(0));
    }

    @Test
    void set() {
        list.add(21);
        list.add(22);
        list.add(23);
        list.add(24);
        list.add(25);

        list.set(2, 7);
        assertEquals(22, list.get(1));
        assertEquals(7, list.get(2));
        assertEquals(24, list.get(3));
    }

    @Test
    void indexOf(){
        list.add(50);
        assertEquals(0, list.indexOf(50));
    }
}