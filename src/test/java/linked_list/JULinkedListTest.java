package linked_list;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Iterator;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;


class JULinkedListTest {
    JULinkedList<Integer> integers;
    int limit;

    @BeforeEach
    void init(){
        limit = 11;
        integers = new JULinkedList<>();
        for (int i = 0; i < limit; i++) {
            integers.add(i);
        }
        System.out.println(integers);
    }

    @AfterEach
    void print(){
        System.out.println(integers.toString());
    }

    @Test
    void size(){
        assertEquals(limit , integers.size());
    }

    @Test
    void isEmpty(){
        integers.clear();
        assertTrue(integers.isEmpty());
    }

    @Test
    void iterable(){
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < limit - 1; i++) {
            assertTrue(iterator.hasNext());
            assertEquals(i, iterator.next());
        }
    }

    @Test
    void clear(){
        integers.clear();

        assertFalse(integers.remove((Object) 21));
    }

    @Test
    void removeObject(){
        assertTrue(integers.remove((Object) 0));
        assertFalse(integers.remove((Object) 0));

        assertTrue(integers.remove((Object) 10));
        assertFalse(integers.remove((Object) 10));
    }

    @Test
    void getIndex(){
        for (int i = 0; i < limit ; i++) {
            assertEquals(i, integers.get(i));
        }

        assertNull(integers.get(69));
    }

    @Test
    void equals(){
        for (int i = 0; i < limit - 1 ; i++) {
            assertTrue(integers.contains(i));
        }

        assertFalse(integers.contains(limit + 1));
    }

    @Test
    void toArray(){
        Integer[] control = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Object[] actual = integers.toArray();
        for (int i = 0; i < control.length; i++) {
            assertEquals(control[i], actual[i]);
        }
    }

    @Test
    void subList(){
        List<Integer> list = integers.subList(4, 8);
        for (int i = 0; i < list.size(); i++) {
            assertEquals(i + 4, list.get(i));
        }
    }

    @Test
    void removeOnIndex(){
        assertEquals(8, integers.remove(8));
        assertEquals(0, integers.remove(0));
        assertEquals(2, integers.remove(1));
    }

    @Test
    void indexOf(){
        JULinkedList<Integer> list = new JULinkedList<>();
        list.add(0);
        list.add(1);
        list.add(4);
        list.add(4);
        list.add(4);

        assertEquals(2, list.indexOf(4));
    }

    @Test
    void lastIndexOf(){
        JULinkedList<Integer> list = new JULinkedList<>();
        list.add(0);
        list.add(1);
        list.add(4);
        list.add(4);
        list.add(4);

        assertEquals(4, list.lastIndexOf(4));
    }

    @Test
    void addElement(){
        integers.add(32);
        assertEquals(32, integers.get(limit));
    }

    @Test
    void addToIndex(){
        assertEquals(4, integers.get(4));
        integers.add(4, 69);
        assertEquals(69, integers.get(4));

    }

    @Test
    void contains(){
        assertTrue(integers.contains(0));
        assertTrue(integers.contains(10));
        assertFalse(integers.contains(11));
    }
}